# fastProject springboot 后端

当前最新版本：1.0.0 （开发中，尚未发布）

[![](https://img.shields.io/hexpm/l/plug.svg)]()
[![](https://img.shields.io/badge/JDK-1.8+-green.svg)]()
[![](https://img.shields.io/badge/springboot-2.4.3-green)]()
[![](https://img.shields.io/badge/mybatis--plus-3.4.0-green)]()
[![](https://img.shields.io/badge/Author-先谢郭嘉-orange.svg)](https://gitee.com/xjb430)
[![](https://img.shields.io/badge/version-1.0.0-brightgreen.svg)](https://gitee.com/xjb430/fastProject)

## 项目介绍

fastProject是一款基于BPM的快速开发平台，拿来即用。

## 开发模块

- [x] 公共模块
- [x] 系统模块
- [x] start启动模块
- [ ] ...... 
 
## 功能模块

## 技术文档

## 技术架构


