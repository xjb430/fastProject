package com.xxgg.common.annotation;
 
import com.xxgg.common.enums.BusinessType;
import io.swagger.annotations.ApiModelProperty;

import java.lang.annotation.*;

/**
 * 自定义操作日志记录注解
 */

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.PARAMETER, ElementType.METHOD})
@Documented
public @interface OpenLog {

    @ApiModelProperty(value = "模块")
    String module() default "";

    @ApiModelProperty(value = "方法名称")
    String name() default "";

    @ApiModelProperty(value = "业务操作类型")
    BusinessType businessType() default BusinessType.OTHER;

    @ApiModelProperty(value = "是否保存请求的参数")
    boolean isSaveRequestData() default true;

    @ApiModelProperty(value = "是否保存响应的参数")
    boolean isSaveResponseData() default true;
}