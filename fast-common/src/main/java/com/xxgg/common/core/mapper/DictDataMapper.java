package com.xxgg.common.core.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xxgg.common.core.model.po.DictData;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 字典数据 Mapper 接口
 * </p>
 *
 * @author 先谢郭嘉
 * @since 2022-07-14 04:21:36
 */
@Mapper
public interface DictDataMapper extends BaseMapper<DictData> {

}
