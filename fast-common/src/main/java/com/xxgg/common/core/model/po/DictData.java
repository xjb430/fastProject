package com.xxgg.common.core.model.po;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xxgg.common.annotation.ExcelDictFormat;
import com.xxgg.common.convert.ExcelDictConvert;
import com.xxgg.common.core.model.BaseModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 字典数据
 * </p>
 *
 * @author 先谢郭嘉
 * @since 2022-07-14 04:21:36
 */
@Setter
@Getter
// 没有注解的字典都不转换
@ExcelIgnoreUnannotated
@TableName("system_dict_data")
@ApiModel(value = "DictData对象", description = "字典数据")
public class DictData extends BaseModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("字典值主键")
    @ExcelProperty(value = "字典值主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("字典排序")
    @ExcelProperty(value = "字典排序")
    @TableField("dict_sort")
    private Integer dictSort;

    @ApiModelProperty("字典标签")
    @ExcelProperty(value = "字典标签")
    @TableField("dict_label")
    private String dictLabel;

    @ApiModelProperty("字典键值")
    @ExcelProperty(value = "字典键值")
    @TableField("dict_value")
    private String dictValue;

    @ApiModelProperty("字典类型")
    @ExcelProperty(value = "字典类型")
    @TableField("dict_type")
    private String dictType;

    @ApiModelProperty("是否默认（Y是 N否）")
    @ExcelProperty(value = "是否默认", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "dict_data_is_default")
    @TableField("is_default")
    private String isDefault;

    @ApiModelProperty("状态（0正常 1停用）")
    @ExcelProperty(value = "字典值状态", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "dict_data_status")
    @TableField("status")
    private String status;

    @ApiModelProperty("备注")
    @ExcelProperty(value = "备注")
    @TableField("remark")
    private String remark;


}
