package com.xxgg.common.core.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

/**
 * @author 先谢郭嘉
 * @version 1.0
 * @description: TODO
 * @date 2023/2/3
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel(value = "BoxVo对象", description = "选择框视图类")
public class SelectBoxVo {

    @ApiModelProperty("标签")
    private String label;

    @ApiModelProperty("值")
    private String value;
}
