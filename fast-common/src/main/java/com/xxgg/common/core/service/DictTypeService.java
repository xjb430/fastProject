package com.xxgg.common.core.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xxgg.common.core.model.po.DictType;

/**
 * <p>
 * 字典类型 服务类
 * </p>
 *
 * @author 先谢郭嘉
 * @since 2022-07-14 04:21:36
 */
public interface DictTypeService extends IService<DictType> {

}
