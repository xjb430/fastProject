package com.xxgg.common.core.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.gson.Gson;
import com.xxgg.common.constant.UserConstants;
import com.xxgg.common.core.mapper.DictDataMapper;
import com.xxgg.common.core.mapper.DictTypeMapper;
import com.xxgg.common.core.model.po.DictData;
import com.xxgg.common.core.service.DictDataService;
import com.xxgg.common.utils.JsonUtils;
import com.xxgg.common.utils.redis.RedisUtil;
import com.xxgg.common.utils.redis.RedissonUtils;
import com.xxgg.common.utils.string.StringUtils;
import org.redisson.Redisson;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 字典数据 服务实现类
 * </p>
 *
 * @author 先谢郭嘉
 * @since 2022-07-14 04:21:36
 */
@Service
public class DictDataServiceImpl extends ServiceImpl<DictDataMapper, DictData> implements DictDataService {

    @Resource
    private DictDataMapper dictDataMapper;

    @Resource
    private DictTypeMapper dictTypeMapper;

    @Override
    public String getDictValue(String dictType, String dictLabel, String separator) {
        StringBuilder propertyString = new StringBuilder();
        // 根据字典类型查询字典数据
        List<DictData> dataList = selectDictDataByType(dictType);

        // 检查字符串和分隔符是否存在交集，且字典数据是否为空
        if (StringUtils.containsAny(dictLabel, separator) && CollUtil.isNotEmpty(dataList)) {
            for (DictData dict : dataList) {
                // 以separator分隔
                for (String label : dictLabel.split(separator)) {
                    // 检查传过来的字典标签是否与数据库中查询的字典标签相同
                    if (label.equals(dict.getDictLabel())) {
                        // 返回字典键值，字符串以分隔符相连
                        propertyString.append(dict.getDictValue() + separator);
                        break;
                    }
                }
            }
        } else {
            // 不存在该分隔符时
            for (DictData dict : dataList) {
                if (dictLabel.equals(dict.getDictLabel())) {
                    return dict.getDictValue();
                }
            }
        }
        return StringUtils.stripEnd(propertyString.toString(), separator);
    }

    @Override
    public String getDictLabel(String dictType, String dictValue, String separator) {
        StringBuilder propertyString = new StringBuilder();
        // 根据字典类型查询字典数据
        List<DictData> dataList = selectDictDataByType(dictType);

        // 检查字符串和分隔符是否存在交集，且字典数据是否为空
        if (StringUtils.containsAny(dictValue, separator) && CollUtil.isNotEmpty(dataList)) {
            for (DictData dict : dataList) {
                // 以separator分隔
                for (String value : dictValue.split(separator)) {
                    // 检查传过来的字典键值是否与数据库中查询的字典键值相同
                    if (value.equals(dict.getDictValue())) {
                        // 返回字典标签，字符串以分隔符相连
                        propertyString.append(dict.getDictLabel()).append(separator);
                        break;
                    }
                }
            }
        } else {
            // 不存在该分隔符时
            for (DictData dict : dataList) {
                if (dictValue.equals(dict.getDictValue())) {
                    return dict.getDictLabel();
                }
            }
        }
        return StringUtils.stripEnd(propertyString.toString(), separator);
    }

    @Override
    public List<DictData> selectDictDataByType(String dictType) {
        // 从redis取出字典数据
        List<DictData> dictDataList = RedissonUtils.getCacheObject(getCacheKey(dictType));
        if (CollUtil.isNotEmpty(dictDataList)) {
            return dictDataList;
        }

        // redis中不存在字典数据，从数据库中查询
        LambdaQueryWrapper<DictData> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(DictData::getStatus, UserConstants.DICT_NORMAL)
                .eq(DictData::getDictType, dictType)
                .orderByAsc(DictData::getDictSort);
        dictDataList = dictDataMapper.selectList(wrapper);

        if (CollUtil.isNotEmpty(dictDataList)) {
            // 将字典数据存入redis，
            RedissonUtils.setCacheObject(getCacheKey(dictType), dictDataList);
            return dictDataList;
        }
        return null;
    }

    /**
     * 设置cache key
     *
     * @param configKey 参数键
     * @return 缓存键key
     */
    String getCacheKey(String configKey) {
        return "dict:" + configKey;
    }
}
