package com.xxgg.common.core.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xxgg.common.core.mapper.DictTypeMapper;
import com.xxgg.common.core.model.po.DictType;
import com.xxgg.common.core.service.DictTypeService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 字典类型 服务实现类
 * </p>
 *
 * @author 先谢郭嘉
 * @since 2022-07-14 04:21:36
 */
@Service
public class DictTypeServiceImpl extends ServiceImpl<DictTypeMapper, DictType> implements DictTypeService {

}
