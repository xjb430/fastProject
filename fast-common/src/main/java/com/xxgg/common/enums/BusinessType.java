package com.xxgg.common.enums;

import com.xxgg.common.core.model.vo.SelectBoxVo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * 业务操作类型
 *
 * @author 先谢郭嘉
 */
@Getter
@AllArgsConstructor
@NoArgsConstructor
public enum BusinessType {

    SELECT("SELECT","查询"),

    PAGE("PAGE","分页查询"),

    INSERT("INSERT","新增"),

    UPDATE("UPDATE","修改"),

    DELETE("DELETE","删除"),

    GRANT("GRANT","授权"),

    EXPORT("EXPORT","导出"),

    IMPORT("IMPORT","导入"),

    FORCE("FORCE","强退"),

    GEN("GEN","生成"),

    CLEAN("CLEAN","清空数据"),

    OTHER("OTHER","其它"),
    ;

    /**
     * 业务操作类型代码
     */
    private String typeCode;

    /**
     * 业务操作类型名称
     */
    private String typeName;

    private String typeCode() {
        return this.typeCode;
    }
    private String typeName() {
        return this.typeName;
    }
    public String getTypeName(){
        return typeName;
    }
    public String getTypeCode(){
        return typeCode;
    }

    public static List<SelectBoxVo> getAllBusinessType(){
        List<SelectBoxVo> list = new ArrayList<>();
        for(BusinessType type : values()){
            list.add(new SelectBoxVo(type.typeCode,type.typeName));
        }
        return list;
    }

    public static String getName(String typeCode){
        for (BusinessType type : values()) {
            if (Objects.equals(type.typeCode, typeCode)) {
                return type.typeName();
            }
        }
        return null;
    }
}
