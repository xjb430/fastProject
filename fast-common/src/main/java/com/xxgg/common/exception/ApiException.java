package com.xxgg.common.exception;

import com.xxgg.common.result.ResultCodeEnum;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * @author 先谢郭嘉
 * @version 1.0
 * @description: 自定义异常
 * @date 2022/3/11
 */
@NoArgsConstructor
public class ApiException extends Exception{
    private ResultCodeEnum errorCode;

    /**
     * 有参的构造方法
     * @param message
     */
    public ApiException(String message){
        super(message);
    }

    public ApiException(ResultCodeEnum errorCode) {
        super(errorCode.getMessage());
        this.errorCode = errorCode;
    }

    /**
     * 用指定的详细信息和原因构造一个新的异常
     * @param message
     * @param cause
     */
    public ApiException(String message, Throwable cause){
        super(message,cause);
    }

    /**
     * 用指定原因构造一个新的异常
     * @param cause
     */
    public ApiException(Throwable cause) {
        super(cause);
    }
}
