package com.xxgg.common.exception;


import com.xxgg.common.result.ResultCodeEnum;

/**
 * 断言处理类，用于抛出各种API异常
 */
public class Asserts {

    public static void fail(String message) throws ApiException {
        throw new ApiException(message);
    }

    public static void fail(ResultCodeEnum errorCode) throws ApiException {
        throw new ApiException(errorCode);
    }

    /**
     * 参数 isTrue 为 true 就抛出异常
     *
     * @param isTrue  是否为true
     * @param message 描述信息
     */
    public static void isTrue(Boolean isTrue, String message) throws ApiException {
        if (isTrue) {
            throw new ApiException(message);
        }
    }

    /**
     * 参数 isFalse 为 false 就抛出异常
     *
     * @param isFalse 是否为false
     * @param message 描述信息
     */
    public static void isFalse(Boolean isFalse, String message) throws ApiException {
        if (!isFalse) {
            throw new ApiException(message);
        }
    }


}
