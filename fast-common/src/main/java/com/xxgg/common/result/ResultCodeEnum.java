package com.xxgg.common.result;

import com.xxgg.common.core.model.vo.SelectBoxVo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @description: 返回状态枚举类
 * @author: 先谢郭嘉
 * @create: 2020-11-22 17:23
 */
@Getter
@AllArgsConstructor
@NoArgsConstructor
public enum  ResultCodeEnum {
    SUCCESS("200", "成功"),
    INVALID_REQUEST("400", "请求无效"),
    ERROR("500", "失败"),

    NOT_LOGIN("0", "当前会话未登录"),
    NOT_TOKEN("-1", "未能读取到有效Token"),
    INVALID_TOKEN("-2", "Token无效"),
    TOKEN_TIMEOUT("-3", "Token已过期"),
    BE_REPLACED("-4", "Token已被顶下线"),
    KICK_OUT("-5", "Token已被踢下线"),


    NOT_ROLE("1", "无此角色"),
    NOT_JUR("2", "无此权限"),
    ;

    /**
     * 状态码
     */
    private String code;
    /**
     * 返回信息
     */
    private String message;

    public static String getMessage(String code){
        ResultCodeEnum[] enums = values();
        for (ResultCodeEnum codeEnum : enums) {
            if (Objects.equals(codeEnum.code, code)) {
                return codeEnum.message();
            }
        }
        return null;
    }

    private String code() {
        return this.code;
    }
    private String message() {
        return this.message;
    }
    public String getMessage(){
        return message;
    }
    public String getCode(){
        return code;
    }

    public static List<SelectBoxVo> getAllCode(){
        List<SelectBoxVo> list = new ArrayList<>();
        ResultCodeEnum[] enums = values();
        for (ResultCodeEnum codeEnum : enums) {
            list.add(new SelectBoxVo(codeEnum.code,codeEnum.message));
        }
        return list;
    }
}
