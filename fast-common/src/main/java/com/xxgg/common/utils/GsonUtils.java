package com.xxgg.common.utils;

import com.google.gson.Gson;

/**
 * @author 先谢郭嘉
 * @version 1.0
 * @description: TODO
 * @date 2022/7/14
 */
public class GsonUtils {

    private final static Gson gson = new Gson();

    /**
     * Object转成JSON数据
     */
    private static String toJson(Object object){
        if(object instanceof Integer || object instanceof Long || object instanceof Float ||
                object instanceof Double || object instanceof Boolean || object instanceof String){
            return String.valueOf(object);
        }
        return gson.toJson(object);
    }

    /**
     * JSON数据，转成Object
     */
    private static <T> T fromJson(String json, Class<T> clazz){
        return gson.fromJson(json, clazz);
    }
}
