package com.xxgg.common.utils;

/**
 * @author 先谢郭嘉
 * @version 1.0
 * @description: TODO
 * @date 2022/7/15
 */
public class JudgeSystem {
    /**
     * 检查系统是否是linux
     * @return
     */
    public static boolean isLinux(){
        return System.getProperty("os.name").toLowerCase().contains("linux");
    }

    /**
     * 检查系统是否是win
     * @return
     */
    public static boolean isWin(){
        return System.getProperty("os.name").toLowerCase().contains("win");
    }

    /**
     * 获取系统名称
     * @return
     */
    public static String getSystemName(){
        return System.getProperty("os.name").toLowerCase();
    }

    public static void main(String[] args) {
        System.out.println(isLinux());
        System.out.println(isWin());
        System.out.println(getSystemName());
    }

}
