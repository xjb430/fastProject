package com.xxgg.start;

import com.xxgg.common.utils.servlet.MacUtils;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.autoconfigure.security.servlet.ManagementWebSecurityAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.EnableScheduling;


@SpringBootApplication(scanBasePackages = {"com.xxgg.**"})
@MapperScan("com.xxgg.**.mapper")
@EnableScheduling
@Slf4j
public class FastStartApplication extends SpringBootServletInitializer {

    @SneakyThrows
    public static void main(String[] args) {
        SpringApplication.run(FastStartApplication.class, args);
        log.info("(♥◠‿◠)ﾉﾞ  SpringBoot 后端启动成功   ლ(´ڡ`ლ)ﾞ");
        log.info("==================当前电脑机器码为 {} ================", MacUtils.getMac());
        log.info("==================当前项目目录 {} ================", System.getProperty("user.dir"));
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(FastStartApplication.class);
    }
}
