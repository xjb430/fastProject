package com.xxgg.start.controller;

import cn.dev33.satoken.annotation.SaIgnore;
import cn.dev33.satoken.secure.SaSecureUtil;
import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.CircleCaptcha;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.xxgg.common.annotation.OpenLog;
import com.xxgg.common.enums.BusinessType;
import com.xxgg.common.exception.ApiException;
import com.xxgg.common.exception.Asserts;
import com.xxgg.common.result.ResultJSON;
import com.xxgg.system.model.po.User;
import com.xxgg.system.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


/**
 * @author 先谢郭嘉
 * @version 1.0
 * @description:
 * @date 2022/3/11
 */
@Slf4j
@Api(tags = "授权认证")
@RestController
@RequestMapping("/start")
public class LoginController {
    @Resource
    UserService userService;
    @Resource
    HttpSession session;
    @Resource
    HttpServletResponse response;

    @SaIgnore
    @OpenLog(module = "授权认证", name = "用户登录", businessType = BusinessType.GRANT)
    @ApiOperationSupport(author = "先谢郭嘉")
    @ApiOperation(value = "用户登录",httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "登录用户名",name = "userName",required = true),
            @ApiImplicitParam(value = "登录密码",name = "password",required = true),
            @ApiImplicitParam(value = "验证码",name = "checkCode",required = true),
    })
    @GetMapping("/login")
    public ResultJSON<Map<String,Object>> loginPc(String userName, String password, String checkCode) throws ApiException {
        String code = (String) session.getAttribute("code");
        Asserts.isFalse(checkCode.equals(code), "验证码错误！");

        // md5加盐加密，盐值为userName
        String loginPasswordSalt = SaSecureUtil.md5BySalt(userName,password);
        User user = userService.lambdaQuery()
                .eq(User::getUserName, userName)
                .eq(User::getPassword, loginPasswordSalt)
                .one();
        Asserts.isTrue(user == null, "账号或密码错误，登陆失败！");

        // 标记当前会话登录的账号id
        StpUtil.login(user.getUserId());

        Map<String,Object> map = new HashMap<>();
        map.put(StpUtil.getTokenInfo().getTokenName(),StpUtil.getTokenInfo().getTokenValue());
        map.put("user",user);
        return ResultJSON.success(map).message("登录成功！");
    }

    @SaIgnore
    @ApiOperationSupport(author = "先谢郭嘉")
    @ApiOperation(value = "获取验证码",httpMethod = "GET")
    @GetMapping("/getCode")
    public void getCode() throws IOException {
        // 利用 hutool 工具，生成验证码图片资源
        CircleCaptcha captcha = CaptchaUtil.createCircleCaptcha(200, 100, 4, 5);
        // 获得生成的验证码字符
        String code = captcha.getCode();
        // 利用 session 来存储验证码
        session.setAttribute("code",code);
        // 将验证码图片的二进制数据写入 response
        captcha.write(response.getOutputStream());
    }


}
