package com.xxgg.start.controller.system;


import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xxgg.common.annotation.OpenLog;
import com.xxgg.common.core.model.po.DictData;
import com.xxgg.common.core.service.DictDataService;
import com.xxgg.common.core.service.DictTypeService;
import com.xxgg.common.enums.BusinessType;
import com.xxgg.common.result.ResultJSON;
import com.xxgg.common.utils.poi.ExcelUtil;
import com.xxgg.common.utils.string.StringUtils;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 数据字典 前端控制器
 * </p>
 *
 * @author 先谢郭嘉
 * @since 2022-07-14
 */
@Validated
@Slf4j
@Api(tags = "数据字典管理")
@RestController
@RequestMapping("/system/dict")
public class DictController {
    @Resource
    private DictTypeService dictTypeService;
    @Resource
    private DictDataService dictDataService;


    @ApiOperation(value = "根据字典类型查询字典数据信息", httpMethod = "GET")
    @ApiImplicitParam(value = "字典类型", name = "dictType", required = true)
    @GetMapping(value = "/type/{dictType}")
    public ResultJSON<List<DictData>> dictType(@PathVariable String dictType) {
        List<DictData> data = dictDataService.selectDictDataByType(dictType);
        if (ObjectUtil.isNull(data)) {
            data = new ArrayList<>();
        }
        return ResultJSON.success(data);
    }

    @SaCheckPermission("dict:page")
    @OpenLog(module = "数据字典管理", name = "分页查询字典数据列表", businessType = BusinessType.PAGE)
    @ApiOperation(value = "分页查询字典数据列表", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "每页数量",name = "size",required = true),
            @ApiImplicitParam(value = "页码",name = "current",required = true),
            @ApiImplicitParam(value = "字典类型",name = "dictType",required = false),
            @ApiImplicitParam(value = "字典状态",name = "status",required = false),
    })
    @GetMapping("/page")
    public ResultJSON<Page<DictData>> dictPage(Integer size, Integer current, String dictType, String status) {
        Page<DictData> data = dictDataService.lambdaQuery()
                .eq(StringUtils.isNotBlank(dictType), DictData::getDictType, dictType)
                .eq(StringUtils.isNotBlank(status), DictData::getStatus, status)
                .orderByDesc(DictData::getDictSort)
                .page(new Page<>(current,size));
        return ResultJSON.success(data);
    }

    @SaCheckPermission("dict:export")
    @OpenLog(module = "数据字典管理", name = "导出字典数据列表", businessType = BusinessType.EXPORT)
    @ApiOperation(value = "导出字典数据列表", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "导出的文件名称", name = "fileName", required = false),
            @ApiImplicitParam(value = "状态",name = "status",required = false),
            @ApiImplicitParam(value = "字典类型",name = "dictType",required = false)
    })
    @GetMapping(value = "/export", produces = "application/octet-stream")
    public void export(String dictType, String status, String fileName, HttpServletResponse response) {
        List<DictData> list = dictDataService.lambdaQuery()
                .eq(StringUtils.isNotBlank(dictType), DictData::getDictType, dictType)
                .eq(StringUtils.isNotNull(status), DictData::getStatus, status)
                .orderByAsc(DictData::getId)
                .orderByAsc(DictData::getDictSort)
                .list();
        if(StringUtils.isEmpty(fileName)) fileName = "字典数据列表";
        ExcelUtil.exportExcel(list, fileName, DictData.class, response);
    }
}
