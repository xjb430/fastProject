package com.xxgg.start.controller.system;


import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.xxgg.common.annotation.OpenLog;
import com.xxgg.common.enums.BusinessType;
import com.xxgg.common.exception.ApiException;
import com.xxgg.common.exception.Asserts;
import com.xxgg.common.result.ResultJSON;
import com.xxgg.system.model.po.Menu;
import com.xxgg.system.service.MenuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 先谢郭嘉
 * @since 2023-01-31 04:33:29
 */
@Validated
@Slf4j
@Api(tags = "菜单管理")
@RestController
@RequestMapping("/system/menu")
public class MenuController {

    @Resource
    MenuService menuService;

    @ApiOperation(value = "获取角色的授权菜单", httpMethod = "GET")
    @ApiImplicitParam(value = "角色id",name = "roleId",required = true)
    @GetMapping("/getMenus")
    public ResultJSON<List<Menu>> getMenuListByRole(Integer roleId) {
        return ResultJSON.success(menuService.getMenuListByRole(roleId));
    }

    @SaCheckPermission("menu:tree")
    @OpenLog(module = "菜单管理", name = "获取菜单树结构", businessType = BusinessType.SELECT)
    @ApiOperation(value = "获取菜单树结构", httpMethod = "GET")
    @GetMapping("/tree")
    public ResultJSON<List<Menu>> menuTree() {
        return ResultJSON.success(menuService.menuTree());
    }

    @SaCheckPermission("menu:add")
    @OpenLog(module = "菜单管理", name = "新增菜单", businessType = BusinessType.INSERT)
    @ApiOperation(value = "新增菜单",httpMethod = "POST")
    @ApiImplicitParam(value = "菜单实体",name = "menu",required = true)
    @PostMapping("/add")
    public ResultJSON<Boolean> addMenu(@RequestBody @Validated Menu menu) {
        menu.setMenuId(IdUtil.simpleUUID());
        return menuService.save(menu) ? ResultJSON.success() : ResultJSON.error();
    }

    @SaCheckPermission("menu:update")
    @OpenLog(module = "菜单管理", name = "编辑菜单", businessType = BusinessType.UPDATE)
    @ApiOperation(value = "编辑菜单",httpMethod = "POST")
    @ApiImplicitParam(value = "菜单实体",name = "menu",required = true)
    @PostMapping("/update")
    public ResultJSON<Boolean> updateMenu(@RequestBody @Validated Menu menu) throws ApiException {
        Asserts.isFalse(StrUtil.isNotBlank(menu.getMenuId()), "菜单id为空！");
        // 根据菜单id查询菜单信息（版本号）
        Menu menuV = menuService.lambdaQuery()
                .select(Menu::getVersion)
                .eq(Menu::getMenuId, menu.getMenuId())
                .one();

        // 修改菜单（版本号需一致）
        LambdaUpdateWrapper<Menu> wrapper = new LambdaUpdateWrapper<>();
        wrapper.eq(Menu::getMenuId, menu.getMenuId())
                .eq(Menu::getVersion, menuV.getVersion());
        menu.setVersion(menu.getVersion() + 1);
        return menuService.update(menu, wrapper) ? ResultJSON.success() : ResultJSON.error();
    }

    @SaCheckPermission("menu:delete")
    @OpenLog(module = "菜单管理", name = "删除菜单", businessType = BusinessType.DELETE)
    @ApiOperation(value = "删除菜单",httpMethod = "DELETE")
    @ApiImplicitParam(value = "菜单id",name = "menuId",required = true)
    @DeleteMapping("/delete")
    public ResultJSON<Boolean> deleteMenu(String menuId) throws ApiException {
        return menuService.deleteMenu(menuId) ? ResultJSON.success() : ResultJSON.error();
    }


}
