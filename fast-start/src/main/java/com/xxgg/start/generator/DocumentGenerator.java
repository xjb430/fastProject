package com.xxgg.start.generator;

import cn.smallbun.screw.core.Configuration;
import cn.smallbun.screw.core.engine.EngineConfig;
import cn.smallbun.screw.core.engine.EngineFileType;
import cn.smallbun.screw.core.engine.EngineTemplateType;
import cn.smallbun.screw.core.execute.DocumentationExecute;
import cn.smallbun.screw.core.process.ProcessConfig;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import javax.sql.DataSource;
import java.util.Arrays;

/**
 * @author 先谢郭嘉
 * @version 1.0
 * @description: TODO
 * @date 2022/7/18
 */
public class DocumentGenerator {

    /**
     * 文档生成
     */
    public static void main(String[] args) {
        documentGeneration();
    }

    // 数据源连接信息
    static String url = "jdbc:mysql://localhost:3306/fast?characterEncoding=UTF-8&serverTimezone=GMT%2B8";
    // 账号
    static String username = "root";
    // 密码
    static String password = "1234";
    // 驱动
    static String driverClassName = "com.mysql.cj.jdbc.Driver";

    // 自定义文件名称
    static String fileName = "数据库文档";
    // 生成文件路径
    static String fileOutputDir = "C:/Users/25012/Desktop/";
    // 忽略表
    static String[] ignoreTableName = new String[]{""};
    // 忽略表前缀
    static String[] ignorePrefix = new String[]{""};
    // 忽略表后缀
    static String[] ignoreSuffix = new String[]{""};

    // 指定表
    static String[] tableNames = new String[]{""};
    // 指定表前缀
    static String[] prefixs = new String[]{""};
    // 指定表后缀
    static String[] suffixs = new String[]{""};


    static void documentGeneration() {
        //数据源
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setDriverClassName(driverClassName);
        hikariConfig.setJdbcUrl(url);
        hikariConfig.setUsername(username);
        hikariConfig.setPassword(password);
        //设置可以获取tables remarks信息
        hikariConfig.addDataSourceProperty("useInformationSchema", "true");
        hikariConfig.setMinimumIdle(2);
        hikariConfig.setMaximumPoolSize(5);
        DataSource dataSource = new HikariDataSource(hikariConfig);
        //生成配置
        EngineConfig engineConfig = EngineConfig.builder()
                //生成文件路径
                .fileOutputDir(fileOutputDir)
                //打开目录
                .openOutputDir(false)
                //文件类型
                .fileType(EngineFileType.HTML)
                //生成模板实现
                .produceType(EngineTemplateType.freemarker)
                //自定义文件名称
                .fileName(fileName).build();

        ProcessConfig processConfig = ProcessConfig.builder()
                //指定生成逻辑、当存在指定表、指定表前缀、指定表后缀时，将生成指定表，其余表不生成、并跳过忽略表配置
                //根据名称指定表生成
                .designatedTableName(Arrays.asList(tableNames))
                //根据表前缀生成
                .designatedTablePrefix(Arrays.asList(prefixs))
                //根据表后缀生成
                .designatedTableSuffix(Arrays.asList(suffixs))
                //忽略表名
                .ignoreTableName(Arrays.asList(ignoreTableName))
                //忽略表前缀
                .ignoreTablePrefix(Arrays.asList(ignorePrefix))
                //忽略表后缀
                .ignoreTableSuffix(Arrays.asList(ignoreSuffix))
                .build();

        //配置
        Configuration config = Configuration.builder()
                //版本
                .version("1.0.0")
                //描述
                .description("数据库设计文档生成")
                //数据源
                .dataSource(dataSource)
                //生成配置
                .engineConfig(engineConfig)
                //生成配置
                .produceConfig(processConfig)
                .build();
        //执行生成
        new DocumentationExecute(config).execute();
    }
}
