package com.xxgg.start.generator;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import com.baomidou.mybatisplus.generator.fill.Column;

import java.sql.SQLException;
import java.util.Collections;

/**
 * <p>
 * 快速生成
 * </p>
 */
public class MyFastAutoGenerator {

    /**
     * 执行 run
     */
    public static void main(String[] args) throws SQLException {

        // 输出目录
        String outputDir = "C:/Users/25012/Desktop/";
        // 表名，支持多个，用逗号分隔
        String[] tableNames = new String[]{"system_menu","system_role","system_role_menu","system_user","system_user_role"};
        // 去掉前缀，支持多个，用逗号分隔
        String tablePrefix = "system_";
        // 去掉后缀，支持多个，用逗号分隔
        String tableSuffix = "";
        // 数据源连接信息
        String url = "jdbc:mysql://localhost:3306/fast?useUnicode=true&useSSL=false&characterEncoding=utf8&serverTimezone=GMT%2B8";
        // 账号
        String username = "root";
        // 密码
        String password = "1234";

        // 配置数据源
        FastAutoGenerator.create(url, username, password)
                // 全局配置
                .globalConfig(builder -> {
                    builder
                            // 覆盖已生成文件
                            .fileOverride()
                            // 禁止打开输出目录
                            .disableOpenDir()
                            // 开启 swagger 模式
                            .enableSwagger()
                            // 定义生成的实体类中日期的类型 TIME_PACK=LocalDateTime  ONLY_DATE=Date
                            .dateType(DateType.ONLY_DATE)
                            //注释日期
                            .commentDate("yyyy-MM-dd hh:mm:ss")
                            // 设置作者
                            .author("先谢郭嘉")
                            // 指定输出目录
                            .outputDir(outputDir + "/src/main/java/");
                })

                // 包配置
                .packageConfig(builder -> {
                    builder
                            // 设置父包名
                            .parent("com.xxgg")
                            // 设置模块包名
                            .moduleName("system")
                            // pojo 实体类包名
                            .entity("model.po")
                            // Service 包名
                            .service("service")
                            // ***ServiceImpl 包名
                            .serviceImpl("service.impl")
                            // Mapper 包名
                            .mapper("mapper")
                            // Mapper XML 包名
                            .xml("mapper")
                            // Controller 包名
                            .controller("controller")
                            // 自定义文件包名
                            .other("model.dto") // 设置dto包名
                            .other("model.vo") // 设置vo包名
                            // 配置 **Mapper.xml 路径信息：项目的 resources 目录的 Mapper 目录下
                            .pathInfo(Collections.singletonMap(OutputFile.mapperXml, outputDir + "/src/main/resources/mapper"));
                })

                // 策略配置
                .strategyConfig(builder -> {
                    builder
                            // 开启大写命名
//                            .enableCapitalMode()
                            // 开启跳过视图
//                            .enableSkipView()
                            // 设置需要生成的表名
                            .addInclude(tableNames)
                            // 设置过滤表前缀，支持多个
                            .addTablePrefix(tablePrefix)
                            // 设置过滤表后缀，支持多个
                            .addTableSuffix(tablePrefix)

                            // Mapper策略配置
                            .mapperBuilder()
                            // 设置父类
                            .superClass(BaseMapper.class)
                            // 格式化 mapper 文件名称
                            .formatMapperFileName("%sMapper")
                            //开启 @Mapper 注解
                            .enableMapperAnnotation()
                            // 格式化 Xml 文件名称
                            .formatXmlFileName("%sMapperXml")

                            // service 策略配置
                            .serviceBuilder()
                            // 格式化 service 接口文件名称，%s进行匹配表名，如 UserService
                            .formatServiceFileName("%sService")
                            // 格式化 service 实现类文件名称，%s进行匹配表名，如 UserServiceImpl
                            .formatServiceImplFileName("%sServiceImpl")

                            // 实体类策略配置
                            .entityBuilder()
                            // 开启 Lombok
                            .enableLombok()
                            // 不实现 Serializable 接口，不生产 SerialVersionUID
//                            .disableSerialVersionUID()
                            // 逻辑删除字段名
                            .logicDeleteColumnName("deleteFlag")
                            // 数据库表映射到实体的命名策略：下划线转驼峰命
                            .naming(NamingStrategy.underline_to_camel)
                            // 数据库表字段映射到实体的命名策略：下划线转驼峰命
                            .columnNaming(NamingStrategy.underline_to_camel)
                            // 乐观锁字段名(数据库)
                            .versionColumnName("version")
                            // 乐观锁字段名(实体)
                            .versionPropertyName("version")
                            // 添加表字段填充，"create_time"字段自动填充为插入时间，"modify_time"字段自动填充为插入修改时间
                            .addTableFills(
                                    new Column("create_time", FieldFill.INSERT),
                                    new Column("modify_time", FieldFill.INSERT_UPDATE)
                            )
                            // 开启生成实体时生成字段注解
                            .enableTableFieldAnnotation()

                            // Controller策略配置
                            .controllerBuilder()
                            // 格式化 Controller 类文件名称，%s进行匹配表名，如 UserController
                            .formatFileName("%sController")
                            // 开启生成 @RestController 控制器
                            .enableRestStyle();
                })

                // 使用Freemarker引擎模板，默认的是Velocity引擎模板
                .templateEngine(new FreemarkerTemplateEngine())
                .execute();
    }
}