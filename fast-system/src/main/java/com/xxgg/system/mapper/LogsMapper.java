package com.xxgg.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xxgg.system.model.po.Logs;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 系统操作日志 Mapper 接口
 * </p>
 *
 * @author 先谢郭嘉
 * @since 2022-07-14 02:04:08
 */
@Mapper
public interface LogsMapper extends BaseMapper<Logs> {

}
