package com.xxgg.system.mapper;

import com.xxgg.system.model.po.Menu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 先谢郭嘉
 * @since 2023-01-31 04:33:29
 */
@Mapper
public interface MenuMapper extends BaseMapper<Menu> {

    /**
     * 根据roleId获取角色的授权菜单
     * @param roleId
     * @return
     */
    @Select("SELECT * FROM system_menu m\n" +
            "INNER JOIN system_role_menu rm\n" +
            "ON rm.menu_id = m.menu_id\n" +
            "INNER JOIN system_role r\n" +
            "ON r.role_id = rm.role_id\n" +
            "WHERE r.role_id = #{roleId}")
    List<Menu> getMenuListByRole(@Param("roleId") Integer roleId);
}
