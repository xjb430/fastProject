package com.xxgg.system.mapper;

import com.xxgg.system.model.po.RoleMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 先谢郭嘉
 * @since 2023-01-31 04:33:30
 */
@Mapper
public interface RoleMenuMapper extends BaseMapper<RoleMenu> {

}
