package com.xxgg.system.mapper;

import com.xxgg.system.model.po.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xxgg.system.model.po.UserRole;
import com.xxgg.system.model.vo.UserRoleVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 先谢郭嘉
 * @since 2023-01-31 04:33:30
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {

    /**
     * 根据userId获取用户和角色信息
     * @param userId
     * @return
     */
    @Select("SELECT * FROM SYSTEM_USER u\n" +
            "INNER JOIN system_user_role ur\n" +
            "ON u.user_id = ur.user_id\n" +
            "INNER JOIN system_role r\n" +
            "ON r.role_id = ur.role_id\n" +
            "WHERE u.user_id = #{userId}")
    UserRoleVo getUserRole(@Param("userId") String userId);


}
