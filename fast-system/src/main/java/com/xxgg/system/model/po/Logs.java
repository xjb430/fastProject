package com.xxgg.system.model.po;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * <p>
 * 系统操作日志
 * </p>
 *
 * @author 先谢郭嘉
 * @since 2022-07-14 02:04:08
 */
@Getter
@Setter
@NoArgsConstructor
@TableName("system_logs")
@ApiModel(value = "Logs对象", description = "系统操作日志")
public class Logs implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("日志主键")
    @TableId(value = "log_id", type = IdType.ASSIGN_UUID)
    private String logId;

    @ApiModelProperty("操作模块")
    @TableField("module")
    private String module;

    @ApiModelProperty("业务操作类型")
    @TableField("business_type")
    private String businessType;

    @ApiModelProperty("请求状态")
    @TableField("request_code")
    private String requestCode;

    @ApiModelProperty("请求方式")
    @TableField("request_method")
    private String requestMethod;

    @ApiModelProperty("方法名称（中文）")
    @TableField("method_name_zh")
    private String methodNameZh;

    @ApiModelProperty("方法名称（英文）")
    @TableField("method_name_en")
    private String methodNameEn;

    @ApiModelProperty("操作人员")
    @TableField("user_name")
    private String userName;

    @ApiModelProperty("请求URL")
    @TableField("url")
    private String url;

    @ApiModelProperty("主机地址")
    @TableField("ip")
    private String ip;

    @ApiModelProperty("操作地点")
    @TableField("location")
    private String location;

    @ApiModelProperty("请求参数")
    @TableField("param")
    private String param;

    @ApiModelProperty("返回参数")
    @TableField("result")
    private String result;

    @ApiModelProperty("操作时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField("log_time")
    private Date logTime;


}
