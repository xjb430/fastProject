package com.xxgg.system.model.po;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.Version;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.xxgg.common.core.model.BaseModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

/**
 * <p>
 * 
 * </p>
 *
 * @author 先谢郭嘉
 * @since 2023-01-31 04:33:29
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@TableName("system_menu")
@ApiModel(value = "Menu对象", description = "")
public class Menu extends BaseModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("菜单id")
    @TableId("menu_id")
    private String menuId;

    @ApiModelProperty("上级菜单id")
    @TableField("menu_pid")
    private String menuPid;

    @ApiModelProperty("菜单类型")
    @TableField("menu_type")
    private Integer menuType;

    @ApiModelProperty("菜单标题")
    @TableField("menu_title")
    private String menuTitle;

    @ApiModelProperty("组件名称")
    @TableField("name")
    private String name;

    @ApiModelProperty("组件")
    @TableField("component")
    private String component;

    @ApiModelProperty("排序")
    @TableField("menu_sort")
    private Integer menuSort;

    @ApiModelProperty("图标")
    @TableField("icon")
    private String icon;

    @ApiModelProperty("链接地址")
    @TableField("path")
    private String path;

    @ApiModelProperty("是否外链")
    @TableField("is_frame")
    private String isFrame;

    @ApiModelProperty("是否隐藏")
    @TableField("is_hide")
    private String isHide;

    @ApiModelProperty("权限码")
    @TableField("permission")
    private String permission;

    @ApiModelProperty("版本号")
    @TableField("version")
    @Version
    private Integer version;

    /**
     * 所有的地区数据
     */
    @TableField(exist = false)
    private List<Menu> children = new ArrayList<Menu>();

    public Menu(List<Menu> children2) {
        this.children = children2;
    }

    /**
     * 建立树形结构
     *
     * @return
     */
    public List<Menu> buildTree() {
        List<Menu> treeAreas = new ArrayList<>();
        for (Menu areaNode : getRootNode()) {
            areaNode = buildChildTree(areaNode);
            treeAreas.add(areaNode);
        }
        return treeAreas;
    }

    /**
     * 递归，建立子树形结构
     *
     * @param pNode
     * @return
     */
    private Menu buildChildTree(Menu pNode) {
        List<Menu> childMenus = new ArrayList<>();
        for (Menu areaNode : children) {
            if (areaNode.getMenuPid().equals(pNode.getMenuId())) {
                childMenus.add(buildChildTree(areaNode));
            }
        }
        pNode.setChildren(childMenus);
        return pNode;
    }

    /**
     * 获取根节点
     *
     * @return
     */
    private List<Menu> getRootNode() {
        List<Menu> rootMenuLists = new ArrayList<>();
        for (Menu areaNode : children) {
            if ("0".equals(areaNode.getMenuPid())) {
                rootMenuLists.add(areaNode);
            }
        }
        return rootMenuLists;
    }
}
