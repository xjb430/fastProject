package com.xxgg.system.model.po;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.Version;
import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.xxgg.common.core.model.BaseModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.NotBlank;

/**
 * <p>
 * 
 * </p>
 *
 * @author 先谢郭嘉
 * @since 2023-01-31 04:33:30
 */
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@TableName("system_role")
@ApiModel(value = "Role对象", description = "")
public class Role extends BaseModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("角色id")
    @TableId(value = "role_id", type = IdType.AUTO)
    private Integer roleId;

    @NotBlank(message = "角色编码不能为空")
    @ApiModelProperty("角色编码")
    @TableField("role_code")
    private String roleCode;

    @NotBlank(message = "角色名称不能为空")
    @ApiModelProperty("角色名称")
    @TableField("role_name")
    private String roleName;

    @ApiModelProperty("描述")
    @TableField("description")
    private String description;

    @ApiModelProperty("版本号")
    @TableField("version")
    @Version
    private Integer version;


}
