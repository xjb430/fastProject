package com.xxgg.system.model.po;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.Version;
import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.xxgg.common.annotation.ExcelDictFormat;
import com.xxgg.common.convert.ExcelDictConvert;
import com.xxgg.common.core.model.BaseModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * <p>
 * 
 * </p>
 *
 * @author 先谢郭嘉
 * @since 2023-01-31 04:33:30
 */
// 没有注解的字典都不转换
@ExcelIgnoreUnannotated
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@TableName("system_user")
@ApiModel(value = "User对象", description = "")
public class User extends BaseModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @ExcelProperty(value = "用户id")
    @ApiModelProperty("用户id")
    @TableId("user_id")
    private String userId;

    @ExcelProperty(value = "用户名")
    @NotBlank(message = "用户名不能为空")
    @ApiModelProperty("用户名")
    @TableField("user_name")
    private String userName;

    @NotBlank(message = "密码不能为空")
    @ApiModelProperty("密码")
    @TableField("password")
    private String password;

    @ExcelProperty(value = "昵称")
    @NotBlank(message = "昵称不能为空")
    @ApiModelProperty("昵称")
    @TableField("nick_name")
    private String nickName;

    @ExcelProperty(value = "性别", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "gender")
    @ApiModelProperty("性别")
    @TableField("gender")
    private String gender;

    @ExcelProperty(value = "手机号")
    @ApiModelProperty("手机号")
    @TableField("phone")
    private String phone;

    @ExcelProperty(value = "邮箱")
    @ApiModelProperty("邮箱")
    @TableField("email")
    private String email;

    @ApiModelProperty("头像名称")
    @TableField("avatar_name")
    private String avatarName;

    @ApiModelProperty("头像路径")
    @TableField("avatar_path")
    private String avatarPath;

    @ExcelProperty(value = "是否为admin账号", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "is_admin")
    @ApiModelProperty("是否为admin账号")
    @TableField("is_admin")
    private String isAdmin;

    @ExcelProperty(value = "是否启用", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "is_enable")
    @ApiModelProperty("是否启用")
    @TableField("is_enable")
    private String isEnable;

    @ApiModelProperty("版本号")
    @TableField("version")
    @Version
    private Integer version;

    @NotNull(message = "角色id不能为空")
    @ApiModelProperty("角色id")
    @TableField(exist = false)
    private Integer roleId;

}
