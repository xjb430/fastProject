package com.xxgg.system.model.vo;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author 先谢郭嘉
 * @since 2023-01-31 04:33:30
 */
@Getter
@Setter
@ApiModel(value = "UserRoleVo对象", description = "UserRoleVo视图类")
public class UserRoleVo implements Serializable {

    // ————————————————————user对象————————————————————
    @ApiModelProperty("用户id")
    @TableField("user_id")
    private String userId;

    @ApiModelProperty("用户名")
    @TableField("user_name")
    private String userName;

    @ApiModelProperty("密码")
    @TableField("password")
    private String password;

    @ApiModelProperty("昵称")
    @TableField("nick_name")
    private String nickName;

    @ApiModelProperty("性别")
    @TableField("gender")
    private String gender;

    @ApiModelProperty("手机号")
    @TableField("phone")
    private String phone;

    @ApiModelProperty("邮箱")
    @TableField("email")
    private String email;

    @ApiModelProperty("头像名称")
    @TableField("avatar_name")
    private String avatarName;

    @ApiModelProperty("头像路径")
    @TableField("avatar_path")
    private String avatarPath;

    @ApiModelProperty("是否为admin账号")
    @TableField("is_admin")
    private String isAdmin;

    @ApiModelProperty("是否启用")
    @TableField("is_enable")
    private String isEnable;

    // ————————————————————role对象————————————————————
    @ApiModelProperty("角色id")
    @TableField("role_id")
    private Integer roleId;

    @ApiModelProperty("角色编码")
    @TableField("role_code")
    private String roleCode;

    @ApiModelProperty("角色名称")
    @TableField("role_name")
    private String roleName;
}
