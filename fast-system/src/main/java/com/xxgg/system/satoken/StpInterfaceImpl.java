package com.xxgg.system.satoken;

import cn.dev33.satoken.stp.StpInterface;
import cn.hutool.core.collection.CollUtil;
import com.xxgg.common.utils.redis.RedissonUtils;
import com.xxgg.system.model.po.Menu;
import com.xxgg.system.model.vo.UserRoleVo;
import com.xxgg.system.service.MenuService;
import com.xxgg.system.service.UserService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 自定义权限验证接口扩展
 */
@Component
public class StpInterfaceImpl implements StpInterface {
    @Resource
    UserService userService;
    @Resource
    MenuService menuService;

    /**
     * 返回一个账号所拥有的权限码集合
     */
    @Override
    public List<String> getPermissionList(Object loginId, String loginType) {
        // 根据用户id获取用户和角色信息
        UserRoleVo userRoleVo = userService.getUserRole((String) loginId);

        // 从redis缓存中查询角色权限码集合
        List<String> permissionRedisList = RedissonUtils.getCacheObject("permission:" + userRoleVo.getRoleCode());
        if (CollUtil.isNotEmpty(permissionRedisList)) {
            return permissionRedisList;
        }

        // redis中不存在角色权限码集合，从数据库中查询
        // 根据角色Id获取角色授权菜单
        List<Menu> menuList = menuService.getMenuListByRole(userRoleVo.getRoleId());
        // 从菜单列表中提取出菜单授权码
        List<String> permissionList = menuList.stream().map(Menu::getPermission).collect(Collectors.toList());
        if (CollUtil.isNotEmpty(permissionList)) {
            // 将角色权限码集合存入redis，
            RedissonUtils.setCacheObject("permission:" + userRoleVo.getRoleCode(), permissionList);
            return permissionList;
        }

        return null;
    }

    /**
     * 返回一个账号所拥有的角色标识集合
     */
    @Override
    public List<String> getRoleList(Object loginId, String loginType) {
        // 根据用户id获取用户和角色信息
        UserRoleVo userRoleVo = userService.getUserRole((String) loginId);

        List<String> list = new ArrayList<>();
        list.add(userRoleVo.getRoleCode());
        return list;
    }

}
