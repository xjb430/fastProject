package com.xxgg.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xxgg.system.model.po.Logs;

/**
 * <p>
 * 系统操作日志 服务类
 * </p>
 *
 * @author 先谢郭嘉
 * @since 2022-07-14 02:04:08
 */
public interface LogsService extends IService<Logs> {

}
