package com.xxgg.system.service;

import com.xxgg.common.exception.ApiException;
import com.xxgg.system.model.po.Menu;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 先谢郭嘉
 * @since 2023-01-31 04:33:29
 */
public interface MenuService extends IService<Menu> {

    /**
     * 根据roleId获取角色的授权菜单
     * @param roleId
     * @return
     */
    List<Menu> getMenuListByRole(Integer roleId);

    /**
     * 构建菜单树结构
     * @return
     */
    List<Menu> menuTree();

    /**
     * 删除菜单
     * @param menuId
     * @return
     * @throws ApiException
     */
    boolean deleteMenu(String menuId) throws ApiException;
}
