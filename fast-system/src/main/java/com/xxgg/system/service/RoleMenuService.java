package com.xxgg.system.service;

import com.xxgg.system.model.po.RoleMenu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 先谢郭嘉
 * @since 2023-01-31 04:33:30
 */
public interface RoleMenuService extends IService<RoleMenu> {

}
