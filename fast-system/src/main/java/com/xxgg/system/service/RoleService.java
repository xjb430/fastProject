package com.xxgg.system.service;

import com.xxgg.common.exception.ApiException;
import com.xxgg.system.model.po.Role;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 先谢郭嘉
 * @since 2023-01-31 04:33:30
 */
public interface RoleService extends IService<Role> {

    /**
     * 获取角色权限码集合
     * @param roleId
     * @return
     */
    List<String> getRolePermission(Integer roleId) throws ApiException;

    /**
     * 角色授权
     * @param roleId
     * @param menuIdList
     * @return
     */
    boolean updateRoleGrant(Integer roleId, List<String> menuIdList) throws ApiException;

}
