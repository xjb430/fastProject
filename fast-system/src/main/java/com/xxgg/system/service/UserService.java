package com.xxgg.system.service;

import com.xxgg.common.exception.ApiException;
import com.xxgg.system.model.po.User;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xxgg.system.model.vo.UserRoleVo;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 先谢郭嘉
 * @since 2023-01-31 04:33:30
 */
public interface UserService extends IService<User> {

    /**
     * 根据userId获取用户和角色信息
     * @param userId
     * @return
     */
    UserRoleVo getUserRole(String userId);

    /**
     * 判断用户名是否存在
     * @param userName
     * @return
     */
    boolean isUserName(String userName);

    /**
     * 新增用户
     * @param user
     * @return
     */
    boolean addUser(User user) throws ApiException;

    /**
     * 编辑用户
     * @param user
     * @return
     */
    boolean updateUser(User user) throws ApiException;
}
