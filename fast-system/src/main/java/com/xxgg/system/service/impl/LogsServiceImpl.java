package com.xxgg.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xxgg.system.mapper.LogsMapper;
import com.xxgg.system.model.po.Logs;
import com.xxgg.system.service.LogsService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统操作日志 服务实现类
 * </p>
 *
 * @author 先谢郭嘉
 * @since 2022-07-14 02:04:08
 */
@Service
public class LogsServiceImpl extends ServiceImpl<LogsMapper, Logs> implements LogsService {

}
